# Description 

This is Tight Binding Calculation for Simple Cubic lattice.

# Requirements to compiling  

  1. gcc / icc (for icc & gcc < 4.0 are not yet tested)
  2. openmp
  3. lapack
  4. gnuplot for plotting



# Compiling program 
 
There are two main programs, in supercell/ folder and exact/ folder
```
#!shell

$ make
```


# Run program 

For supercell code
```
#!shell
$ ./tbcal 
```
For analytic code
```
#!shell
$ ./deltaN
```
#Supported program 
It is allows profiling program *gprof* and debugger program *gdb*



# Contributor 
Cica Gustiani