#!/usr/bin/gnuplot
set terminal png size 1024,768

set ylabel "n(E)"
#set key outside 
unset key
set style line 1 lt rgb "red" lw 3
set style line 2 lt rgb "violet" lw 3
set style line 3 lt rgb "green" lw 3
set style line 4 lt rgb "blue" lw 3
set xrange [-2:10]

set output "dos-imp-18.png"
set xlabel "E"
set title "With Impurity DOS 18x18x18, 5832 atoms"
plot for[col=2:5833] 'dos-imp-18.dat' u 1:col w l smooth unique
unset output

