typedef struct{
  int Nsite;  // Number of sites
  int L; //length in one dimension 
  int R; //The supercell
  unsigned* idx_i;  //Configuration of lattice
  unsigned* idx_j;  //Configuration of lattice
  unsigned* idx_k;  //Configuration of lattice
  _Complex double *H;  //Hamiltonian
  int stat; //status 
}lattice;

void setupLattice(lattice *Lat, int L);
void confLattice(lattice *Lat);
void setupAtomConf(char *conf, int site);
void setupHamiltonian(double kx, double ky, double kz, double a, double hop_coef, double e0, _Complex double *H, lattice* Lat);
void freeLattice(lattice *Lat);
void print_idx(lattice* Lat);
void print_mat(double* matrix, int dimm, int dimn);
void print_cmat(_Complex double* matrix, int dimm, int dimn);
void print_H_idx(lattice* Lat);
