/**
 * Tight binding calculation with Green's Function
 * @author Cica Gustiani
 * @requires setup.c
 */
#define PI 3.14159265358979323846

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <complex.h>
#include <omp.h>
#include "matvecop.h"
#include "setup.h"

void zheev_(char*, char*, int*, _Complex double*, int*, double*, _Complex double*, int*, double*, int*);
void progress(int i, int n_thread);

/*Parameters*/
int Lsite = 4;   //Number of site in one dimension, in a unit cell
int N_k   = 201;   //k-points, must be odd number
double W  = -0.5;  //Hopping coefficient
double a  = 1.0;   //Lattice constant
double e0 = 2.0;   //Energy

int main(int argc, char* argv[])
{
  lattice *Lat;
  double *e_k;
  _Complex double *c_k;
  _Complex double *work;
  _Complex double *rwork;
  double kx, ky, kz, dk;
  int lwork, info, N12, done;

  Lat= malloc(sizeof(lattice));
  setupLattice(Lat, Lsite);

  omp_set_num_threads(8);
  double start = omp_get_wtime();
 /*now printing part*/
  char fname1[20];
  char str_lsite[2]; sprintf(str_lsite,"%d",Lsite);
  strcpy(fname1,"bandL-");strcat(fname1,str_lsite);strcat(fname1,".dat");
  FILE *f1 = fopen(fname1, "w");

  kx = 0., ky = 0., kz = 0.;
  dk = N_k == 0 ? 0.0 : 8*PI/(Lsite*(N_k-1));

  N12 = (int)N_k/2;
  lwork = Lat->Nsite*Lat->Nsite;
  done = 0;
#pragma omp parallel private(c_k, e_k, work, rwork, info, kx, ky, kz) shared(dk, N12, e0, Lat)
  {
    //get basis and Eigen energy
    c_k   = (_Complex double*)malloc(sizeof(_Complex double)*Lat->Nsite*Lat->Nsite);
    e_k   = (double*)malloc(sizeof(double)*Lat->Nsite);
    work  = (_Complex double*)malloc(sizeof(_Complex double)*lwork);
    rwork = (_Complex double*)malloc(sizeof(_Complex double)*(3*Lat->Nsite-2));
#pragma omp for reduction(+ : done)
    for(int k_x=0; k_x<N_k; k_x++){
        for(int k_y=0; k_y<N_k; k_y++){
            for(int k_z=0; k_z<N_k; k_z++){
                kx = dk*(k_x-N12); ky = dk*(k_y-N12); kz = dk*(k_z-N12);
                setupHamiltonian(kx, ky, kz, a, W, e0, c_k, Lat);
                zheev_("V", "L", &Lat->Nsite, c_k, &Lat->Nsite, e_k, work, &lwork, rwork, &info);
                //printing to files
                if(k_x==k_y && k_z==k_y){
#pragma omp critical (band)
                {
                  fprintf(f1,"%8.3f", sqrt(kx*kx+ky*ky+kz*kz));
                  for(int j=0; j<Lat->Nsite; j++){
                      fprintf(f1,"%10.5f", e_k[j]);
                  }
                  fprintf(f1,"\n");
                 }
              }
                done++;
                if(omp_get_thread_num() == 0 )progress(done,omp_get_num_threads());
           }
       }
    }
    free(work); free(rwork); free(c_k);free(e_k);
  }
  fclose(f1);
  freeLattice(Lat);free(Lat);
  return 0;
}


void progress(int i, int n_thread){
  int k_tot = pow(N_k-1,3);
  int step = (int)k_tot/25;
  if(i % step == 0){
      printf("progress: %i %  \n", (int)i*100/k_tot);
  }
}
