/* Matrix and vector operations */
/* Vector-vector multiplication */

/* transpose a matrix, input is destroyed */
void transpose(double *mat, int dimr, int dimc){
  double* mat_ori = malloc(sizeof(double)*dimr*dimc);
  memcpy(mat_ori, mat, sizeof(double)*dimr*dimc);
  int i, j;
  for(int n=0; n<dimr*dimc; n++){
    i = n/dimr;
    j = n%dimr;
    mat[n] = mat_ori[dimc*j + i];
  }
  free(mat_ori);
}

/* transpose a matrix, input is destroyed */
void ctranspose(_Complex double *mat, int dimr, int dimc){
  _Complex double* mat_ori = malloc(sizeof(_Complex double)*dimr*dimc);
  memcpy(mat_ori, mat, sizeof(_Complex double)*dimr*dimc);
  int i, j;
  for(int n=0; n<dimr*dimc; n++){
    i = n/dimr;
    j = n%dimr;
    mat[n] = mat_ori[dimc*j + i];
  }
  free(mat_ori);
}


void setzero(double *mat, int dimc, int dimr){
  for(int i=0; i<dimc*dimr; i++){
      mat[i] = 0.0;
  }
}

void csetzero(_Complex double *mat, int dimc, int dimr){
  int size = dimc*dimr;
  for(int i=0; i<size; i++){
      mat[i] = 0.0;
  }
}

