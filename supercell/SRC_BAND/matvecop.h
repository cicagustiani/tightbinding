void transpose(double *mat, int dimr, int dimc);
void setzero(double *mat, int dimc, int dimr);
void csetzero(_Complex double *mat, int dimc, int dimr);
void ctranspose(_Complex double *mat, int dimr, int dimc);
