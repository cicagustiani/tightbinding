#!/usr/bin/env zsh
#BSUB -J dos-4  # job name
#BSUB -oo dos_U.txt # job output (use %J for job ID)
#BSUB -W 30:00     # limits in hours:minutes
#BSUB -M 320000       # memory in MB
#BSUB -n 32         # number of slots
#BSUB -a "bcs openmp"
#BSUB -N
#BSUB -u cicagustiani@gmail.com
./tbcal 20 10
