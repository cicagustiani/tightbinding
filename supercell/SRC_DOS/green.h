/*
 * green.h
 * contains green function related
 */
#define PI 3.14159265358979323846

void G_sum(_Complex double* G_k, double* E_n, _Complex double z, int size, _Complex double* c_k);
void G_sum_diag(_Complex double *G_tt, double *e_k, _Complex double z, int size, _Complex double *c_k);



