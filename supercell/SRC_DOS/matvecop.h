extern void transpose(double *mat, int dimr, int dimc);
extern void setzero(double *mat, int dimc, int dimr);
extern void csetzero(_Complex double *mat, int dimc, int dimr);
extern void ctranspose(_Complex double *mat, int dimr, int dimc);
