/**
 Setup initial lattice, Hamiltonian
 also printing
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <complex.h>
#include <math.h>
#include "matvecop.h"
#include "setup.h"

/**
* @param Lat The lattice structure  
* @param L The size of supercell 
*/
void setupLattice(lattice *Lat, int L)
{
  Lat->stat = 1;
  Lat->L = L;
  Lat->R = L - 1;
  Lat->Nsite = Lat->L * Lat->L* Lat->L;
  Lat->idx_i = malloc(Lat->Nsite*sizeof(unsigned));
  Lat->idx_j = malloc(Lat->Nsite*sizeof(unsigned));
  Lat->idx_k = malloc(Lat->Nsite*sizeof(unsigned));
  //Lat->H = malloc(Lat->Nsite*Lat->Nsite*sizeof(_Complex double));
  confLattice(Lat);
}

/*
 Setup configuration of lattice. Map configuration from 3D lattice to one single number   
*/ 
void confLattice(lattice* Lat)
{
  for(int i=0; i<Lat->L; i++){
    for(int j=0; j<Lat->L; j++){
      for(int k=0; k<Lat->L; k++){
        Lat->idx_i[k + j*Lat->L + i*Lat->L*Lat->L] =i;
        Lat->idx_j[k + j*Lat->L + i*Lat->L*Lat->L] = j;
        Lat->idx_k[k + j*Lat->L + i*Lat->L*Lat->L] = k;
     }   
    }	
  }
}

void setupAtomConf(char* conf, int site){
  int times;
  double rand_num = 0.0;
  srand(time(NULL));
  for(int i=0; i<site; i++){
    times = i*i;
    while(times--){
      rand_num = (double)rand()/RAND_MAX;
    }
    conf[i] = (rand_num > 0.5) ? 'A': 'B';
  }
}

/*
Setup Hamiltonian and conf.
The disorder is on the site (subtitution)
*/
void setupHamiltonian(double kx, double ky, double kz, double a, double hop_coef, double e0, _Complex double *H, lattice *Lat)
{
  int dx, dy, dz, dr;
  csetzero(H, Lat->Nsite, Lat->Nsite);
    if(Lat->Nsite == 1){
        H[0] = e0 + 2*hop_coef*(cos(kx*a) + cos(ky*a) + cos(kz*a));
    }else{
      for(int i=0; i<Lat->Nsite; i++){
          for(int j=0; j<Lat->Nsite; j++){
              dx = abs(Lat->idx_i[i]-Lat->idx_i[j]);  if(dx == Lat->R)dx = 1;
              dy = abs(Lat->idx_j[i]-Lat->idx_j[j]);  if(dy == Lat->R)dy = 1;
              dz = abs(Lat->idx_k[i]-Lat->idx_k[j]);  if(dz == Lat->R)dz = 1;
              dr = dx + dy + dz;
              if(dr == 0){
                  H[i*Lat->Nsite + j] =  e0;
              }else if(dr == 1){
                if(dx == 1){
                  H[i*Lat->Nsite + j] = abs(Lat->idx_i[i]-Lat->idx_i[j]) == 1 ? hop_coef : 0.;
                  if(abs(Lat->idx_i[i]-(Lat->idx_i[j]+Lat->L)) == 1){
                      H[i*Lat->Nsite + j] += hop_coef*cexp(I*kx*Lat->L*a);
                  }else if(abs(Lat->idx_i[i]-(Lat->idx_i[j]-Lat->L)) == 1){
                      H[i*Lat->Nsite + j] += hop_coef*cexp(-I*kx*Lat->L*a);
                  }
                }else if(dy == 1){
                  H[i*Lat->Nsite + j] = abs(Lat->idx_j[i]-Lat->idx_j[j]) == 1 ? hop_coef : 0.;
                  if(abs(Lat->idx_j[i]-(Lat->idx_j[j]+Lat->L)) == 1){
                      H[i*Lat->Nsite + j] += hop_coef*cexp(I*ky*Lat->L*a);
                  }else if(abs(Lat->idx_j[i]-(Lat->idx_j[j]-Lat->L)) == 1){
                      H[i*Lat->Nsite + j] +=hop_coef*cexp(-I*ky*Lat->L*a);
                  }
                }else if(dz == 1){
                  H[i*Lat->Nsite + j] = abs(Lat->idx_k[i]-Lat->idx_k[j]) == 1 ? hop_coef : 0.;
                  if(abs(Lat->idx_k[i]-(Lat->idx_k[j]+Lat->L)) == 1){
                      H[i*Lat->Nsite + j] +=hop_coef*cexp(I*kz*Lat->L*a);
                  }else if(abs(Lat->idx_k[i]-(Lat->idx_k[j]-Lat->L)) == 1){
                      H[i*Lat->Nsite + j] +=hop_coef*cexp(-I*kz*Lat->L*a);
                  }
                }

              }
          }
       }
    }
}

/**
 Free memories of lattice 
*/
void freeLattice(lattice *Lat)
{
  if(Lat->stat == 1)
  {
    free(Lat->idx_i);
    free(Lat->idx_j);
    free(Lat->idx_k);
   // free(Lat->H);
    Lat->stat = 0;
  }
  else
  {
    printf("Lattice is not yet allocated!\n");
    exit(1);
  }
}

//PRINTING
/*Print index */
void print_idx(lattice* Lat)
{
  int L = Lat->L;
  for(int i=0; i<L; i++){
    for(int j=0; j<L; j++){
      for(int k=0; k<L; k++){
        printf("idx[%u][%u][%u]\t= %i \n",Lat->idx_i[k+j*L+i*L*L],Lat->idx_j[k+j*L+i*L*L],Lat->idx_k[k+j*L+i*L*L],k+j*L+i*L*L);
      }
    }
  }
}

/*Print matrix form*/
void print_mat(double* matrix, int dimm, int dimn)
{
  for(int i=0; i<dimm; i++){
    for(int j=0; j<dimn; j++){
     printf("%8.4f",matrix[dimn*i+j]);
    }
    printf("\n");
  }
}

void print_cmat(_Complex double* matrix, int dimm, int dimn)
{
  char sign;
  for(int i=0; i<dimm; i++){
    for(int j=0; j<dimn; j++){
        if(cimag(matrix[dimn*i+j]) != 0.){
            sign = cimag(matrix[dimn*i+j]) < 0 ? '-' : '+';
            printf("%9.4f%1ci%9.4f",creal(matrix[dimn*i+j]),sign,abs(cimag(matrix[dimn*i+j])) );
        }else{
            printf("%16.4f",creal(matrix[dimn*i+j]));
        }
    }
    printf("\n");
  }
}

/*Print H with index atom */
void print_H_idx(lattice* Lat)
{
  for(int i=0; i<Lat->Nsite; i++){
    for(int j=0; j<Lat->Nsite; j++){
     printf("(%i,%i,%i)(%i,%i,%i)  ",Lat->idx_i[i],Lat->idx_j[i], Lat->idx_k[i],Lat->idx_i[j],\
         Lat->idx_j[j],Lat->idx_k[j] );
        printf("(%i,%i) ", i, j);
    }
    printf("\n");
  }
}

