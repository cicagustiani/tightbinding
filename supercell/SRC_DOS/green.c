#include <complex.h>
/**
 * Fill the G_{t,t'}(Z) matrix
 * Input mainly eigenvector of Hamiltonian
 * c_k is in column order
 */
void G_sum(_Complex double *G_k, double *E_k, _Complex double z, int size, _Complex double *c_k){
  ctranspose(c_k, size, size);
  csetzero(G_k, size, size);
  _Complex double* ck_n = (_Complex double *) malloc(sizeof(_Complex double)*size*size);
  for(int i=0; i<size; i++){
        //get ck_n
        for(int z1=0; z1<size; z1++){
          for(int z2=0; z2<size; z2++){
             ck_n[z1*size+z2] = c_k[i*size+z1]*conj(c_k[i*size+z2])/(z - E_k[i]);
          //   ck_n[z1*size+z2] = (creal(c_k[z1*size+z2])*creal(c_k[z1*size+z2])+cimag(c_k[z1*size+z2])*cimag(c_k[z1*size+z2]))*(creal(z) - E_k[i] - I*cimag(z))/(creal(z) - E_k[i] - cimag(z)*cimag(z));
          }
        }
        for(int j=0; j<size; j++){
            for(int k=0; k<size; k++){
                //add to G_k
                 G_k[j*size+k] += ck_n[j*size+k];
            }
        }
  }
  free(ck_n);
}

void G_sum_diag(_Complex double *G_tt, double *e_k, _Complex double z, int size, _Complex double *c_k){
  csetzero(G_tt, 1, size);
  for(int i=0; i<size; i++){
      for(int j=0; j<size; j++){
       G_tt[i] += c_k[i*size + j] * conj(c_k[i*size+j])/(z - e_k[i]);
      }
  }
}


