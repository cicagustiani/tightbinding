#!/usr/bin/gnuplot
set term postscript eps enhanced color font 'Times-Roman,24'
 
set style line 1 lc rgb '#000000' lw 3 lt 1
set style line 2 lc rgb "#AC00E6" lw 2 lt 2
set style line 3 lc rgb "#E63900" lw 1 lt 3
set style line 4 lc rgb "#245BFF" lw 3 lt 1
set style line 5 lc rgb "#E63900" lw 3 lt 1
set style line 6 lc rgb "#00E639" lw 5 lt 1
set style line 7 lc rgb "#00B386" lw 3 lt 1
set style line 8 lc rgb "#0086B3" lw 3 lt 1
set style line 9 lc rgb "#B3002D" lw 3 lt 1
set style line 10 lc rgb "#B38600"lw 3 lt 1
set style line 11 lc rgb "#600080"lw 3 lt 1
set style line 12 lc rgb "#800060"lw 3 lt 1

set output "band.eps"
set multiplot
set ylabel "{/Symbol e}(k)"
#set key outside
#set key samplen 2
#set key font ",20"
unset key
set xrange [0:pi]
#set yrange [-1:1]
set xtics ("{/Symbol G}" 0)
#set ytics -1.0,0.5,1.0
set size 0.5,1
set origin 0,0
plot 'bandX-1.dat' u 1:2 w l ls 6 smooth unique t "1 atom",\
     for[col=3:9] 'bandX-2.dat' u 1:col ls 2 smooth unique notitle,\
     'bandX-2.dat' u 1:2 w l ls 2 t "2 atom" smooth unique,\
     for[col=3:17] 'bandX-4.dat' u 1:col w l ls 3 notitle smooth unique,\
     'bandX-4.dat' u 1:2 w l ls 3 t "4 atom" smooth unique


set size 0.45,1
set origin 0.39,0.0
unset ytics
unset ylabel
set xtics ("X {/Symbol G}" 0, "L" sqrt(3)*pi)
set xrange [0:sqrt(3)*pi]
plot 'bandL-1.dat' u 1:2 w l ls 6 smooth unique t "1 atom",\
     for[col=3:9] 'bandL-2.dat' u 1:col ls 2 smooth unique notitle,\
     'bandL-2.dat' u 1:2 w l ls 2 t "2 atom" smooth unique,\
     for[col=3:17] 'bandL-4.dat' u 1:col ls 3 smooth unique notitle,\
     'bandL-4.dat' u 1:2 w l ls 3 t "4 atom" smooth unique



