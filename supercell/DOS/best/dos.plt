#!/usr/bin/gnuplot
set term postscript eps enhanced

set ylabel "n(E)"
#set key outside 
unset key
set style line 1 lt rgb "red" lw 3
set style line 2 lt rgb "violet" lw 3
set style line 3 lt rgb "green" lw 3
set style line 4 lt rgb "blue" lw 3
set xrange [-2:6]

set output "dos-0.eps"
set xlabel "E"
plot 'dos-1.dat' u 1:2 w l ls 1 smooth unique
unset output

