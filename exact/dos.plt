#!/usr/bin/gnuplot
set terminal postscript eps enhanced color font 'Helvetica,15'

set style line 1 lc rgb '#000000' lw 3 lt 1
set style line 2 lc rgb "#AC00E6" lw 3 lt 1
set style line 3 lc rgb "#E63900" lw 3 lt 1
set style line 4 lc rgb "#245BFF" lw 3 lt 1
set style line 5 lc rgb "#E63900" lw 3 lt 1
set style line 6 lc rgb "#00E639" lw 3 lt 1
set style line 7 lc rgb "#00B386" lw 3 lt 1
set style line 8 lc rgb "#0086B3" lw 3 lt 1
set style line 9 lc rgb "#B3002D"  lw 3 lt 1
set style line 10 lc rgb "#B38600"  lw 3 lt 1
set style line 11 lc rgb "#600080"  lw 3 lt 1
set style line 12 lc rgb "#800060"   lw 3 lt 1

set output "dos.eps"

set xlabel "{/Symbol e}"
set ylabel "DOS"
set xzeroaxis lt 1 lc "black" 
#set xrange [-2:10]
#set yrange [0:0.7]
plot 'dos0.dat' u 1:2 w l ls 1 smooth unique t "{/Symbol e}_{imp} = 0",\
     'dos.dat' u 1:2 w l ls 2 smooth unique t "{/Symbol e}_{imp} = 2.1",\
     'dos.dat' u 1:3 w l ls 3 smooth unique t "{/Symbol e}_{imp} = 2.5",\
     'dos.dat' u 1:4 w l ls 4 smooth unique t "{/Symbol e}_{imp} = 3.0",\
     'dos.dat' u 1:5 w l ls 5 smooth unique t "{/Symbol e}_{imp} = 4.0",\
     'dos.dat' u 1:6 w l ls 6 smooth unique t "{/Symbol e}_{imp} = 4.5",\
     'dos.dat' u 1:7 w l ls 7 smooth unique t "{/Symbol e}_{imp} = 5.0",\
     'dos.dat' u 1:8 w l ls 8 smooth unique t "{/Symbol e}_{imp} = 6.0",\
     'dos.dat' u 1:9 w l ls 9 smooth unique t "{/Symbol e}_{imp} = 7.0",\
     'dos.dat' u 1:10 w l ls 10 smooth unique t "{/Symbol e}_{imp} = 8.0"
