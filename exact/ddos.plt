#!/usr/bin/gnuplot
set terminal postscript eps enhanced color font 'Helvetica,15'

set pointintervalbox 2
set style line 1 lc rgb '#000000' lw 2 lt 1
set style line 2 lc rgb "#AC00E6" lw 2 lt 1
set style line 3 lc rgb "#E63900" lw 2 lt 1
set style line 4 lc rgb "#245BFF" lw 2 lt 1
set style line 5 lc rgb "#E63900" lw 2 lt 1
set style line 6 lc rgb "#00E639" lw 2 lt 1
set style line 7 lc rgb "#00B386" lw 2 lt 1
set style line 8 lc rgb "#0086B3" lw 3 lt 1
set style line 9 lc rgb "#B3002D" lw 3 lt 1
set style line 10 lc rgb "#B38600" lw 3 lt 1
set style line 11 lc rgb "#600080" lw 3 lt 1
set style line 12 lc rgb "#800060" lw 3 lt 1
set xrange [0 : 50]
set samples 500
set output "ddos.eps"
set xlabel "|R|"
set ylabel "n^0(R) - n(R)"
set ytics 0.05
set xzeroaxis lt 0 
unset ylabel
dn(x) = a*cos(2*k_f*x+psi)
set multiplot
unset title
unset xlabel
# "Inside band, {/Symbol e}_{imp} = 8.0, {/Symbol e} = 0.0"

set key right bottom
set key font ",12"
set key spacing 0.5
set key samplen -1

set yrange [-0.015:0.015]
k_f = 1.571 #e = 0.00
psi = 0.75*pi
a = 0.009
set size 1,0.35
set origin 0,-0.01
plot 'dn.dat' u 1:2 w l ls 2 notitle smooth unique,\
      ''   u 1:2 w points ls 2 notitle,\
      dn(x) t "{/Symbol e}=0.0" ls 4

set xtics format ""	
unset xlabel
k_f = 2.133
psi = pi
a = 0.005
set origin 0.0,0.3
plot 'dn.dat' u 1:3 w l ls 2 notitle smooth unique,\
      ''   u 1:3 w points ls 2 notitle,\
      dn(x) t "{/Symbol e} = 1.0" ls 4
           
set yrange [-0.015:0.015]           
k_f = 1.257
psi = pi*0.53
a = 0.01
set origin 0.0,0.6
plot 'dn-03.dat' u 1:2 w l ls 2 notitle smooth unique,\
      ''   u 1:2 w points ls 2 notitle,\
      dn(x) t "{/Symbol e} = -0.3"  ls 4
      
    

