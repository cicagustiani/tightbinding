#!/usr/bin/gnuplot
set terminal postscript eps enhanced color font 'Helvetica,15'

set style line 1 lc rgb '#0060ad' lt 1 lw 2 pt 2 pi -1 ps 1.5
set pointintervalbox 2
set style line 2 lt rgb "violet" lw 3
set style line 3 lt rgb "green" lw 3
set style line 4 lt rgb "blue" lw 3
set style line 5 lt rgb "brown" lw 3
set xrange [1 : 150]
set output "ddos.eps"

set xlabel "|R|"
set ylabel "n(R) - n^0(R)"
set xzeroaxis lt 0 

set size 1,0.7
set yrange [-0.00005:0.00005]
plot 'dn.dat' u 1:2 w points ls 1 t "e =-0.50613",\
     '' u 1:2 w l ls 1 smooth unique
          


