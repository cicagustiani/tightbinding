double W0  = -0.5;    //Hopping coefficient
double a   = 1.0;     //Lattice length
double e0  = 2.0;     //Energy
double e_imp = 3.2;   //Impurity energy
double gamm    = 5e-2;//gamma
int Rg[3]      = {150, 0, 0}; //R for expansion G
int N_k = 50;       //k-points
double e[1] = {-0.50613 }; //observed energy
int N_e = 1; //How many energy observed
