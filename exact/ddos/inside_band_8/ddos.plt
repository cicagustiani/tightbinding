#!/usr/bin/gnuplot
set terminal postscript eps enhanced color font 'Helvetica,15'

set pointintervalbox 2
set style line 1 lc rgb '#000000' lw 2 lt 1
set style line 2 lc rgb "#AC00E6" lw 2 lt 1
set style line 3 lc rgb "#E63900" lw 2 lt 1
set style line 4 lc rgb "#245BFF" lw 2 lt 1
set style line 5 lc rgb "#E63900" lw 2 lt 1
set style line 6 lc rgb "#00E639" lw 2 lt 1
set style line 7 lc rgb "#00B386" lw 3 lt 1
set style line 8 lc rgb "#0086B3" lw 3 lt 1
set style line 9 lc rgb "#B3002D"  lw 3 lt 1
set style line 10 lc rgb "#B38600"  lw 3 lt 1
set style line 11 lc rgb "#600080"  lw 3 lt 1
set style line 12 lc rgb "#800060"   lw 3 lt 1
set xrange [1 : 100]
set output "ddos.eps"

set xlabel "|R|"
set ylabel "n^0(R) - n(R)"
set xzeroaxis lt 0 

k_f = 0.1
psi = pi/20
dn(x) = cos(2*k_f*x+psi)

set yrange [-0.005:0.005]
set title "Inside band, {/Symbol e}_{imp} = 8.0"
plot 'dn.dat' u 1:2 w l ls 2 smooth unique t "{/Symbol e} = -0.50613",\
     'dn.dat' u 1:2 w l ls 3 smooth unique t "{/Symbol e} = 0.0",\
     'dn.dat' u 1:3 w l ls 4 smooth unique t "{/Symbol e} = 1.0",\
     'dn.dat' u 1:4 w l ls 5 smooth unique t "{/Symbol e} = 2.0",\
     'dn.dat' u 1:4 w l ls 6 smooth unique t "{/Symbol e} = 4.0"

