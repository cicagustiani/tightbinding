/**
 * Tight binding calculation with Green's Function
 * @author Cica Gustiani
 * @requires setup.c
 */
#define PI 3.14159265358979323846

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <complex.h>
#include <omp.h>
#include <math.h>
#include "matvecop.h"
#include "setup.h"
#include "green.h"

void zheev_(char*, char*, int*, _Complex double*, int*, double*, _Complex double*, int*, double*, int*);
void progress(int i, int n_thread, int N_e);

/*Parameters*/
int Lsite = 3;   //Number of site in one dimension, in a unit cell
char calc = 'c'; // 'c' || 'i'
int N_k   = 10;
double W  = -0.5;  //Hopping coefficient
double a  = 1.0;   //Lattice constant
double e0 = 2.0;   //Energy
double gamm = 5e-2; //Gamma
double e_mesh[2] = {-2.,10.};
double de = 0.1;
double e_imp = 8.0;
int main(int argc, char* argv[])
{
  lattice *Lat;
  double *e_k;
  _Complex double *c_k;
  _Complex double *work;
  double *rwork;
  _Complex double *Gtt_diag; //diagonal part of G_tt
  _Complex double *Gtt;
  double kx, ky, kz, dk, n_i, cons, dos_converge;
  int lwork, info, done;
  int N_e;
  _Complex double z;

  Lat= malloc(sizeof(lattice));
  setupLattice(Lat, Lsite);

  omp_set_num_threads(8);
  double start = omp_get_wtime();
 /*now printing part*/
  char fname1[20];
  char str_lsite[2]; sprintf(str_lsite,"%d",Lsite);
  if(calc == 'c'){
      strcpy(fname1,"dos-clean-");
  }else{
      strcpy(fname1,"dos-imp-");
  }
  strcat(fname1,str_lsite);strcat(fname1,".dat");
  FILE *f1 = fopen(fname1, "w");
#pragma omp parallel
  {
    if(omp_get_thread_num()== 0)
        printf("Number of thread: %i\n", omp_get_num_threads());
  }
  printf("Executing for %i atoms per unit cell .................\n", Lsite*Lsite*Lsite);
  printf("Gamma value %g \n", gamm);
  printf("Lattice size: %i x %i x %i\n", Lsite, Lsite, Lsite);
  printf("Number of K_points: %i x %i x %i\n", N_k, N_k, N_k);
  printf("Size of energy mesh: %g\n", de);

  kx = 0., ky = 0., kz = 0.;
  dk = N_k == 1 ? 0.0 : 2*PI/(Lsite*(N_k));
  cons = N_k == 1 ? 1.0 : 1.0/(N_k*N_k*N_k);

  N_e = (int)((e_mesh[1]-e_mesh[0])/de)+1;
  lwork = 2*Lat->Nsite;
  done = 0;
  dos_converge = 0.0;
#pragma omp parallel private(c_k, e_k, work, rwork, Gtt_diag, Gtt, info, kx, ky, kz, z) shared(dk, e0, Lat)
  {
    //get basis and Eigen energy
    c_k   = (_Complex double *)malloc(sizeof(_Complex double)*Lat->Nsite*Lat->Nsite);
    e_k   = (double *)malloc(sizeof(double)*Lat->Nsite);
    work  = (_Complex double *)malloc(sizeof(_Complex double)*lwork);
    rwork = (double *)malloc(sizeof(double)*(3*Lat->Nsite-2));
    Gtt_diag = (_Complex double *)malloc(sizeof(_Complex double)*Lat->Nsite);
    Gtt   = (_Complex double *)malloc(sizeof(_Complex double)*Lat->Nsite);
#pragma omp for reduction(+ : done)
  for(int i=0; i<N_e; i++){
      z = i*de + e_mesh[0] + _Complex_I*gamm;
      csetzero(Gtt, 1, Lat->Nsite);
      for(int k_x=0; k_x<N_k; k_x++){
          for(int k_y=0; k_y<N_k; k_y++){
              for(int k_z=0; k_z<N_k; k_z++){
                  kx = dk*k_x; ky = dk*k_y; kz = dk*k_z;
                  if(calc == 'c'){
                      setupHamiltonian(kx, ky, kz, a, W, e0, c_k, Lat, NULL);
                  }else{
                      setupHamiltonian(kx, ky, kz, a, W, e0, c_k, Lat, &e_imp);
                  }
                  zheev_("V", "L", &Lat->Nsite, c_k, &Lat->Nsite, e_k, work, &lwork, rwork, &info);
                  G_sum_diag(Gtt_diag, e_k, z, Lat->Nsite, c_k);
                  for(int n=0; n<Lat->Nsite; n++){ //integrate for k
                      Gtt[n] += Gtt_diag[n]*cons;
                  }
             }
         }
      }
#pragma omp critical (dos)
{
    fprintf(f1,"%10.4f%5s",creal(z),"");
    for(int n=0; n<Lat->Nsite; n++){
        n_i = -cimag(Gtt[n])/PI;
        fprintf(f1, "%12.6f",n_i);
        dos_converge += n_i*de;
    }
    fprintf(f1, "\n");
}
      done++;
      progress(done,omp_get_num_threads(),N_e);
  }
    free(work); free(rwork); free(c_k);free(e_k);
    free(Gtt_diag);free(Gtt);
  }
  fclose(f1);
  freeLattice(Lat);free(Lat);

#pragma omp parallel
{
    if(omp_get_thread_num()== 0){
        /* Toc */
        double end = omp_get_wtime();
        printf("Elapsed time: %g \n", end-start);
        printf("DOS converge %g \n", dos_converge);
        printf("Calculation is successful (^_<)b \n");
        printf("\n");
    }
}

  return 0;
}


void progress(int done, int n_thread, int N_e){
  int percentage = (int)done*100*n_thread/N_e;
  if(percentage % 20 == 0){
    printf("progress: %i %% \n", percentage);
  }
}
