#define PI 3.14159265358979323846

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>
#include <math.h>
#include <omp.h>

void csetzero(_Complex double *mat, int dimc, int dimr);
void progress(int done, int n_thread, int iter);

char calc = 'q';    // e | r | q
double W0 = -0.5;   //Hopping coefficient
double a  = 1.0;    //Lattice length
double e0 = 2.0;    //Energy
double gamm = 1e-2; //gamma
int Rg[3] = {8, 0, 0}; //R for expansion G
int N_k = 100;       //k-points
double e[1] = {7.31}; //observed energy for delta n
int N_e = 1; //How many energy observed
double e_imp = 7.0;
double e_mesh[2] = {5.22,8.53}; // The energy observed for DOS, Q
double de_mesh = 0.01; // The step size of energy
int n_imp = 9;
double imp[9] = { 2.1, 2.5, 3.0, 4.0, 4.5, 5.0, 6.0, 7.0, 8.0};

int main(void)
{
  /*tic*/
  double start = omp_get_wtime();
  omp_set_num_threads(8);

  _Complex double* G0_00;
  _Complex double* G0_0R;
  _Complex double* Z = (_Complex double *) malloc(sizeof(_Complex double)*N_e);
  for(int i=0; i<N_e; i++) Z[i] = e[i] + _Complex_I*gamm;
  double dk = (2*PI/(a))/N_k;
  double weight_int = pow((1*a/N_k),3);

  double ne_mesh_ =(e_mesh[1] - e_mesh[0])/de_mesh;
  int ne_mesh = (int) ne_mesh_;
  if(ne_mesh%2 == 0){ //must be odd
      ne_mesh+=1;
      e_mesh[1]+=de_mesh;
  }
  //calculate DOS
  if(calc == 'e'){
      double* n_total = (double *)calloc(n_imp, sizeof(double));
      double* n_prev = (double *)calloc(n_imp, sizeof(double));
      double* n_new = (double *)calloc(n_imp, sizeof(double));
      char fname1[20];
      char str_lsite[2]; sprintf(str_lsite,"%d",e_imp);
      strcpy(fname1,"dos-");strcat(fname1,str_lsite);strcat(fname1,".dat");
      FILE *fdos = fopen(fname1, "w");
    #pragma omp parallel
    {
      _Complex double g0_00;
      _Complex double g00;
      double kx,ky,kz,H_k,n_i,e_i;
    #pragma omp for
      for(int i=0; i<ne_mesh; i++){
          e_i = de_mesh*i + e_mesh[0];
          g0_00 = 0.0;
          g00 = 0.0;
          memcpy(n_prev, n_new, sizeof(double)*n_imp);
          for(int k_x=0; k_x<N_k; k_x++){ //loop all over BZ
             for(int k_y=0; k_y<N_k; k_y++){
               for(int k_z=0; k_z<N_k; k_z++){
                 kx = k_x*dk-PI/a;
                 ky = k_y*dk-PI/a;
                 kz = k_z*dk-PI/a;
                 H_k = e0 + 2*W0*(cos(kx*a) + cos(ky*a) + cos(kz*a));
                 g0_00 += weight_int/(e_i + _Complex_I*gamm - H_k);
               }
             }
           }
    #pragma omp critical (dos)
          {
            fprintf(fdos, "%10.4f", e_i);
            for(int n=0; n<n_imp; n++){
                g00 = g0_00/(1.0 - g0_00*(imp[n]-e0)); //e_imp - e0, e_imp = e0 + 0.5n
                n_i = -cimag(g00)/PI;
                n_new[n] = -cimag(g00)/PI;
                fprintf(fdos, "%15.8f", n_new[n]);
                n_total[n] += 0.5*(n_new[n] + n_prev[n])*de_mesh;
            }
            fprintf(fdos,"\n");
          }
      }
    }
    printf("DOS convergence\n");
    for(int i=0; i<n_imp; i++)printf("%15.5f",n_total[i]);
    printf("\n");
    fclose(fdos);
    free(n_total);
  }

  else if(calc == 'r'){
      char fname1[20];
      char str_lsite[2]; sprintf(str_lsite,"%d",(int)e[0]);
      strcpy(fname1,"dn-");strcat(fname1,str_lsite);strcat(fname1,".dat");
      FILE *fdn = fopen(fname1, "w");
#pragma omp parallel private (G0_00, G0_0R)
  {
    G0_00 = (_Complex double *) malloc(sizeof(_Complex double)*N_e); //G^0_00
    G0_0R = (_Complex double *) malloc(sizeof(_Complex double)*N_e); //G^0_0R
    _Complex double dG_RR;
   double H_k, kx, ky, kz, R, n_i;
   if(omp_get_thread_num()== 0){
       printf("Number of thread: %i\n", omp_get_num_threads());
   }
#pragma omp for
      for(int r1=0; r1<Rg[0]+1; r1++){
        for(int r2=0; r2<Rg[1]+1; r2++){
          for(int r3=0; r3<Rg[2]+1; r3++){
            csetzero(G0_00,1,N_e);
            csetzero(G0_0R,1,N_e);
             for(int k_x=0; k_x<N_k; k_x++){ //loop all over BZ
                for(int k_y=0; k_y<N_k; k_y++){
                  for(int k_z=0; k_z<N_k; k_z++){
                    kx = k_x*dk-PI/a; ky = k_y*dk-PI/a; kz = k_z*dk-PI/a;
                    H_k = e0 + 2*W0*(cos(kx*a) + cos(ky*a) + cos(kz*a));
                    for(int i=0; i<N_e; i++){ //There are N_e observed energy
                      G0_00[i] += weight_int/(Z[i] - H_k);
                      G0_0R[i] += weight_int*cexp(_Complex_I*(kx*r1+ky*r2+kz*r3)*a)/(Z[i] - H_k);
                    }
                  }
                }
              }
             /* print to file*/
#pragma omp critical (ddos)
             {
             R = sqrt(r1*r1+r2*r2+r3*r3);
             fprintf(fdn, "%10.4f%5s", R,"");
             for(int i=0; i<N_e; i++){
               dG_RR = G0_0R[i]*(e_imp-e0)*G0_0R[i]/(1-(G0_00[i]*(e_imp-e0)));
               n_i = -cimag(dG_RR)/PI;
               fprintf(fdn,"%15.10f", n_i);
             }
             fprintf(fdn,"\n");
            }
          }
        }
      }
      free(G0_00);free(G0_0R);
  }
  fclose(fdn);
  free(Z);
  }

  else if(calc == 'q'){
      int done = 0;
      char fname1[20];
      char str_lsite[2]; sprintf(str_lsite,"%d",(int)e[0]);
      strcpy(fname1,"q-");strcat(fname1,str_lsite);strcat(fname1,".dat");
      FILE *fq = fopen(fname1, "w");
#pragma omp parallel private (G0_00, G0_0R)
  {
    G0_00 = (_Complex double *) malloc(sizeof(_Complex double)*ne_mesh); //G^0_00
    G0_0R = (_Complex double *) malloc(sizeof(_Complex double)*ne_mesh); //G^0_0R
    _Complex double dG_RR;
   double H_k, kx, ky, kz, R, e_i, q, n_i;
   if(omp_get_thread_num()== 0){
       printf("Number of thread: %i\n", omp_get_num_threads());
   }
#pragma omp for
      for(int r1=0; r1<Rg[0]+1; r1++){
        for(int r2=0; r2<Rg[1]+1; r2++){
          for(int r3=0; r3<Rg[2]+1; r3++){
            csetzero(G0_00,1,N_e);
            csetzero(G0_0R,1,N_e);
             for(int k_x=0; k_x<N_k; k_x++){ //loop all over BZ
                for(int k_y=0; k_y<N_k; k_y++){
                  for(int k_z=0; k_z<N_k; k_z++){
                    kx = k_x*dk-PI/a; ky = k_y*dk-PI/a; kz = k_z*dk-PI/a;
                    H_k = e0 + 2*W0*(cos(kx*a) + cos(ky*a) + cos(kz*a));
                    for(int i=0; i<ne_mesh; i++){ //There are N_e observed energy
                      e_i = de_mesh*i + e_mesh[0];
                      G0_00[i] += weight_int/(e_i + _Complex_I*gamm - H_k);
                      G0_0R[i] += weight_int*cexp(_Complex_I*(kx*r1+ky*r2+kz*r3)*a)/(e_i + _Complex_I*gamm - H_k);
                    }
                  }
                }
              }
             /* print to file*/
#pragma omp critical (q)
             {
             R = r1;
             q = 0;
             for(int i=1; i<ne_mesh; i++){
               dG_RR = G0_0R[i]*(e_imp-e0)*G0_0R[i]/(1-(G0_00[i]*(e_imp-e0)));
               n_i = -cimag(dG_RR)/PI;
               if(i == 0 || i == ne_mesh-1){
                   q += n_i;
               }else if(i % 2 == 0){
                   q += 2*n_i;
               }else{
                   q += 4*n_i;
               }
             }
             q = q * de_mesh / 3.0;
             fprintf(fq, "%15.4f%15.10f\n", R,q);
             done++;
             progress(done, omp_get_num_threads(), ne_mesh);
            }
          }
        }
      }
      free(G0_00);free(G0_0R);
  }
  fclose(fq);
  }

#pragma omp parallel
{
    if(omp_get_thread_num()== 0){
        /* Toc */
        double end = omp_get_wtime();
        printf("Elapsed time: %g \n", end-start);
        printf("Calculation is successful (^_<)b \n");
        printf("\n");
    }
}

  return 0;
}

/* Set each matrix element to zero */
void csetzero(_Complex double *mat, int dimc, int dimr){
  for(int i=0; i<dimc*dimr; i++){
      mat[i] = 0.0 ;
  }
}

void progress(int done, int n_thread, int iter){
  int percentage = (int)done*100*n_thread/iter;
  printf("progress: %i %% \n", percentage);

}




