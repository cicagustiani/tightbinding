#!/usr/bin/gnuplot
set term postscript eps enhanced color

set ylabel "E(k)"
#set key outside 
unset key
set style line 1 lt rgb "red" lw 3
set style line 2 lt rgb "violet" lw 3
set style line 3 lt rgb "green" lw 3
set style line 4 lt rgb "blue" lw 3
set xrange [0:3.1416]
set size 0.5,1

set output "p100.ps"
set xlabel "(0,0,0)(PI/A,0,0)"
plot 'b100-1.dat' u 1:2 w l ls 1 smooth unique,\
     for[col=2:9] 'b100-2.dat' u 1:col w l ls 2 smooth unique,\
     for[col=2:65] 'b100-4.dat' u 1:col w l ls 4 smooth unique
unset output

set output "p111.ps"
set xlabel "(0,0,0)(PI/A,PI/A,PI/A)"
plot 'b111-1.dat' u 1:2 w l ls 1 smooth unique,\
     for[col=2:9] 'b111-2.dat' u 1:col w l ls 2 smooth unique,\
     for[col=2:65] 'b111-4.dat' u 1:col w l ls 4 smooth unique
 unset output
 
set output "p110.ps"
set xlabel "(0,0,0)(PI/A,PI/A,0)"
plot 'b110-1.dat' u 1:2 w l ls 1 smooth unique,\
     for[col=2:9] 'b110-2.dat' u 1:col w l ls 2 smooth unique,\
     for[col=2:65] 'b111-4.dat' u 1:col w l ls 4 smooth unique
 unset output